ABOUT
-----

This is a small module that generates a text file containing the output of 
view. The file is generates whenever a specified content type is created or 
updated.

To use, first create a and ordinary View, then specify the content type, and 
the view. Set an optional save path and file extension.

The module will provide the node id  to be uses as an argument for the View.